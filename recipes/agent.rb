#
# (C) ITculate, Inc. 2015-2017
# All rights reserved
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

api_key = node['itculate']['agent_config']['api']['api_key']
api_secret = node['itculate']['agent_config']['api']['api_secret']
version = node['itculate']['version']
agent_path = '/opt/itculate-agent'

# TODO: Not working yet...
ruby_block 'itculate-api-key-unset' do
  block do
    raise "Set ['itculate']['agent_config']['api']['api_key'] and ['itculate']['agent_config']['api']['api_secret']"
  end
  only_if { api_key.nil? or api_secret.nil? }
end


# Make sure Python 2.7 is installed and current
python_runtime '2'


# Install perquisites
case node['platform_family']
  when 'debian'
    %w(build-essential libssl-dev libffi-dev python-dev python-setuptools supervisor).each do |p|
      package p do
        action :install
      end
    end

  else
    fail('Unsupported OS')
end


# Add a group and user for the agent to run
group 'itsdk' do
  action :create
end


user 'itsdk' do
  comment 'ITculate agent'
  gid 'itsdk'
  home agent_path
  manage_home true
  shell '/usr/sbin/nologin'
  system true
  action :create
end

# Download the agent (of the specified version) from BitBucket
ark 'itculate-agent' do
  action :put
  path '/opt' # ark appends the resource name to the path to create the full path to the extracted files
  owner 'itsdk'
  group 'itsdk'
  mode '0755'
  url "https://bitbucket.org/itculate/itculate-agent/get/#{version}.tar.gz"
end

# Install our virutalenv and requirements
python_virtualenv "#{agent_path}/venv" do
  user 'itsdk'
end

pip_requirements "#{agent_path}/requirements.txt"


# Make the log directory
directory '/var/log/itculate' do
  owner 'itsdk'
  group 'itsdk'
  mode '0755'
  action :create
end

directory '/etc/itculate' do
  owner 'itsdk'
  group 'itsdk'
  mode '0555'
  action :create
end


# Configure the agent
chef_gem 'inifile' do
  compile_time true
end

require 'inifile'
agent_config = node['itculate']['agent_config']
agent_config = agent_config.each_pair do |k,v|
  next unless v.respond_to?(:each_pair)
  v.dup.delete_if do
    |k,v| v.nil?
  end
end
agent_config = ::IniFile.new(content: agent_config)

template '/etc/itculate/agent.conf' do
  source 'agent.conf.erb'
  owner 'itsdk'
  group 'itsdk'
  mode '0555'
  variables({
    agent_config: agent_config
  })
  notifies :run, 'execute[reload_supervisor]'
end


# Configure the supervisor and reload
template '/etc/supervisor/conf.d/itculate-agent.conf' do
  source 'itculate-agent.conf.erb'
  mode '0555'
  notifies :run, 'execute[reload_supervisor]'
end

execute 'reload_supervisor' do
  command "supervisorctl reload"
  action :nothing
end
