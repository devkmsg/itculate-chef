Where to Find the Code
======================
To submit issues and patches please visit https://bitbucket.org/itculate/itculate-chef.
The code is licensed under the Apache License 2.0 (see  LICENSE for details).

ITculate Cookbook
=================

Chef recipes to deploy the ITculate agent and configure it automatically.

**NB: This README may refer to features that are not released yet. Please check the README of the
git tag/the gem version you're using for your version's documentation**

Requirements
============
- chef >= 10.14

Platforms
---------

* Debian
* Ubuntu

Cookbooks
---------

The following Opscode cookbooks are dependencies:

* `build-essential`
* `poise-python`
